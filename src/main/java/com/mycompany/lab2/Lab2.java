/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentplayer = 'x';
    static int row, col;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            System.out.print("You want to Play?(yes/no): ");
            Scanner kb = new Scanner(System.in);
            String Playagian = kb.next();
            if (Playagian.equals("yes")) {
                initializeTable();
                while (true) {
                    printtable();
                    printTurn();
                    inputRowCol();

                    if (isWin()) {
                        printtable();
                        printWin();
                        break;
                    } else if (isDraw()) {
                        printtable();
                        printDraw();
                        break;
                    }
                    switPlayer();
                }
            } else;
            {
                break;
            }

        }

    }

    private static void printWelcome() {
        System.out.println("Welcome Game OX!!");
    }

    private static void printtable() {
        int i;
        int j;
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
        System.out.println(currentplayer + " Turn!");
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.println("Please input row col:");
            row = kb.nextInt();
            col = kb.nextInt();
            if (isValidMove()) {
                table[row - 1][col - 1] = currentplayer;
                return;
            } else {
                System.out.println("Invalid move! Try again.");
            }
        }

    }

    private static void switPlayer() {
        if (currentplayer == 'x') {
            currentplayer = 'o';
        } else {
            currentplayer = 'x';
        }
    }

    private static boolean isWin() {
        if (checkRow() || checkCol() || checkA()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println(currentplayer + " Win!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentplayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentplayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkA() {
        if (table[1][1] == currentplayer && ((table[0][0] == currentplayer && table[2][2] == currentplayer)
                || (table[0][2] == currentplayer && table[2][0] == currentplayer))) {
            return true;
        }
        return false;
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("Draw");
    }

    private static void initializeTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';
            }
        }
    }

    private static boolean isValidMove() {
        return (row >= 1 && row <= 3 && col >= 1 && col <= 3 && table[row - 1][col - 1] == '-');
    }

}
